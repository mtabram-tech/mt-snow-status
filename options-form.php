<?php
/**
 * Created by IntelliJ IDEA.
 * User: jsanford
 * Date: 12/3/15
 * Time: 10:34 PM
 */

$options = [
    "trails" => [
        "West Side" => [
            "Maineiac" => ["status"=>"open","groomed"=>"1","snowmaking"=>"0","skill"=>"blue"],
            "Mahousoc Meadows" => ["status"=>"open","groomed"=>"1","snowmaking"=>"0","skill"=>"green"]
        ],
        "Main Side" => [
            "boris" => ["status"=>"open","groomed"=>"1","snowmaking"=>"1","skill"=>"black"],
            "Dudley Do Right" => ["status"=>"open","groomed"=>"0","snowmaking"=>"0","skill"=>"green"],
            "Fearless Leader" => ["status"=>"closed","groomed"=>"0","snowmaking"=>"1","skill"=>"black"],
            "Rockie's Run" => ["status"=>"closed","groomed"=>"0","snowmaking"=>"0","skill"=>"doubleblack"]
        ]
    ],
    "imagelist" => [
        "black"=>"trail-black.png",
        "doubleblack"=>"trail-doubleblack.png",
        "groomed"=>"trail-groomed.png",
        "terrain"=>"trail-terrain.png",
        "blue"=>"trail-blue.png",
        "event"=>"trail-event.png",
        "open"=>"trail-open.png",
        "treeski"=>"trail-tree.png",
        "closed"=>"trail-closed.png",
        "green"=>"trail-green.png",
        "snowmaking"=>"trail-snowmaking.png"
    ],
    "snowdata" => [
        "base" => 12,
        "new" => 7.5,
        "notes" => "Until nature provides, the snow guns are busy.  Opening this weekend."
    ]
];

$trails = $options["trails"];
$imagelist = $options["imagelist"];
$snowdata = $options["snowdata"];

?>

<div class="wrap">
    <h2>Mt Abram Trail Status</h2>
<form action="options.php" method="post">
    <input id="snow-base" type="text" value="<?php $snowdata['base'];?>"/><br/>
    <input id="snow-new" type="text" value="<?php $snowdata['new'];?>"/><br/>
    <input id="snow-notes" type="text" value="<? $snowdata['notes'] ?>"/><br/>

    <?php

    foreach ( $trails as $side => $info ){
        echo "<h2>" . $side . "</h2>";
        echo "<ul>";
        foreach ( $info as $trail => $tdata ){
            echo "<li>Trail Open:<input type='radio' name='side-trail-status' value='open'/> Closed:<input type='radio' name='side-trail-status' value='closed'/>";
            echo " Groomed:<input type='checkbox' name='side-trail-groomed'/> Snowmaking:<input type='checkbox' name='side-trail-snowmaking'/> ";
            echo "</li>";
        }
        echo "</ul>";
    }

    ?>

    <input type="submit"/>
</form>

</div>

