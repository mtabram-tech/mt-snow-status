<style>
#site-header{ display:none; }
div#site-header:before{ display:none; }
div#page:before{ display:none; }
#masthead-header{ display:none; }
#secondary{ display:none; }
#colophon{ display:none; }
#comments{ display:none; }
.entry-content .edit-link a.post-edit-link{ display:none; }
header{ display:none; }
.site-content div.entry-content{ 
	width:100%;
	max-width:100%;
	}
.entry-content #trailstatusprintmaintable{
	max-width:700px;
	white-space:nowrap;
	font-size:small;
	border:none;
	padding:0;
}

.entry-content #trailstatusprintmaintable td{
	border:none;
}

#trailstatusprintdiv .trailstatusprinttable caption{
	border-spacing: 20px 2px;
}

.trailstatusprinttable caption{
	text-align:center;
	font-weight:bold;
	font-size:large;
}

.entry-content #trailstatusprintdiv table.trailstatusprinttable td.trailnameprint{
	padding: 1px;
	padding-right:10px;
	padding-left:10px;
	border: 1px solid black;
}


.mt-snow-status table{
	margin-bottom:0px;
}
#trailstatusprintdiv{
	max-width:700px;
}
</style>
<div style="visibility: visible;">

<h1 ><img src="//cdn.mtabram.com/wp-content/uploads/2015/03/19123156/Logo.jpg" width="150" style="float:left"/>Mt Abram Trail Status</h1>
<div class="entry-content">
    <div class="mt-snow-status">
        <div class="snow-info"><p style="margin-bottom:5px; width:685px;"><?= stripslashes($snowdata["notes"]) ?>
</p>
<div style="clear:both"></div>
<hr style="margin-bottom:5px; margin-top:0px;"/>
            Base: <?= $snowdata["base"] ?>     New:<?= $snowdata["new"] ?>     Trails Open:<?= $snowdata["trailsopen"] ?>
        </div>
        <div id="trailstatusprintdiv">
            <table  id="trailstatusprintmaintable">
                <tr>
                    <td>
                        <table class="trailstatusprinttable">
                            <?php $side = "Main Side"; ?>
                            <caption>Main Side</caption>
                            <tbody>
                            <tr><?= trail_print_row( $side, "LowerRound-a-bout" ); ?>
                                <?=  trail_print_row( $side, "HoppityHooper" ); ?>
			    </tr>
			    <tr>
                            <?= trail_print_row( $side, "WilksWay" ); ?>
                                <?=  trail_print_row( $side, "Round-a-Bout" ); ?>
			    </tr>
			    <tr>
                            <?= trail_print_row( $side, "Upsidasium" ); ?>
                                <?=  trail_print_row( $side, "BigT-Line" ); ?>
			    </tr>
			    <tr>
                            <?= trail_print_row( $side, "OneToday" ); ?>
                                <?=  trail_print_row( $side, "PeabodysPass" ); ?>
			    </tr>
			    <tr>
                            <?= trail_print_row( $side, "LowerEasyRider" ); ?>
                                <?=  trail_print_row( $side, "ChickenChute" ); ?>
			    </tr>
			    <tr>
                            <?= trail_print_row( $side, "TaylorsTreat" ); ?>
                                <?=  trail_print_row( $side, "ShermansPass" ); ?>
			    </tr>
			    <tr>
                            <?= trail_print_row( $side, "UpperSweeper" ); ?>
                                <?=  trail_print_row( $side, "NatashasNiche" ); ?>
			    </tr>
			    <tr>
                            <?= trail_print_row( $side, "Sweeper" ); ?>
                                <?=  trail_print_row( $side, "Presto" ); ?>
			    </tr>
			    <tr>
                            <?= trail_print_row( $side, "FrostbiteFalls" ); ?>
                                <?=  trail_print_row( $side, "McClainsLane" ); ?>
			    </tr>
			    <tr>
                            <?= trail_print_row( $side, "R2D2" ); ?>
                                <?=  trail_print_row( $side, "UpperFracturedFairytales" ); ?>
			    </tr>
			    <tr>
                            <?= trail_print_row( $side, "BullwinklesHorn" ); ?>
                                <?=  trail_print_row( $side, "LowerFracturedFairytales" ); ?>
			    </tr>
			    <tr>
                            <?= trail_print_row( $side, "BullwinklesOtherHorn" ); ?>
                                <?=  trail_print_row( $side, "Lallypalooza" ); ?>
			    </tr>
			    <tr>
                            <?= trail_print_row( $side, "UpperEasyRider" ); ?>
                                <?=  trail_print_row( $side, "TuckersTumble" ); ?>
			    </tr>
			    <tr>
                            <?= trail_print_row( $side, "Snowfields" ); ?>
                                <?=  trail_print_row( $side, "UpperRockysRun" ); ?>
			    </tr>
			    <tr>
                            <?= trail_print_row( $side, "Snake" ); ?>
                                <?=  trail_print_row( $side, "LowerRockysRun" ); ?>
			    </tr>
			    <tr>
                            <?= trail_print_row( $side, "UpperBullwinkle" ); ?>
                                <?=  trail_print_row( $side, "SnidleyWhiplash" ); ?>
			    </tr>
			    <tr>
                            <?= trail_print_row( $side, "Bullwinkle" ); ?>
                                <?=  trail_print_row( $side, "FearlessLeader" ); ?>
			    </tr>
			    <tr>
                            <?= trail_print_row( $side, "Dudley–Do-Right" ); ?>
                                <?=  trail_print_row( $side, "BorisBadenov" ); ?>
			    </tr>
			    <tr>
                            <?= trail_print_row( $side, "DudleyPark" ); ?>
                                <?=  trail_print_row( $side, "ZephyrGlades" ); ?>
			    </tr>
			    <tr>
                            <?= trail_print_row( $side, "MaybellesTail" ); ?>
                                <?=  trail_print_row( $side, "LowerZephyrGlades" ); ?>
			    </tr>
			    <tr>
                            <?= trail_print_row( $side, "CrossStreet" ); ?>
                                <?=  trail_print_row( $side, "TheRAT" ); ?>
			    </tr>
			    <tr>
                            <?= trail_print_row( $side, "NellsWay" ); ?>
                                <?=  trail_print_row( $side, "TheCliff" ); ?>
			    </tr>
			    <tr>
                            <?= trail_print_row( $side, "PatsPass" ); ?>
                                <?=  trail_print_row( $side, "TheZone" ); ?>
			    </tr>
			    <tr>
                                <?= trail_print_row( $side, "WassomattaU" ); ?>
				<td>&nbsp;</td>
			    </tr>
			    <tr>
			    </tr>
                            </tbody>
                        </table>
                    </td>
                    <td>
                        <table class="trailstatusprinttable">
                            <?php $side = "West Side"; ?>
				<caption>West Side</caption>
                            <tbody>
				<tr>
                                <?=  trail_print_row( $side, "Hayroad" ); ?>
				</tr>
				<tr>
                                <?=  trail_print_row( $side, "Skyline" ); ?>
				</tr>
				<tr>
                                <?=  trail_print_row( $side, "MahoosucMeadow" ); ?>
				</tr>
				<tr>
                                <?=  trail_print_row( $side, "Polkadot" ); ?>
				</tr>
				<tr>
                                <?=  trail_print_row( $side, "GidneysGulch" ); ?>
				</tr>
				<tr>
                                <?=  trail_print_row( $side, "Egomaineah" ); ?>
				</tr>
				<tr>
                                <?=  trail_print_row( $side, "FlyingSquirrelTerrainPark" ); ?>
				</tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
</div>
