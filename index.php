<?php
/*
* Plugin Name: Mountain Trail Status
* Description: Snow and Trail Status plugin for alpine ski mountains 
* Version: .1
* Author: Jason Sanford
* Author URI: http://www.acornrain.com
*/

add_shortcode('mt-snow-status', 'print_status');

wp_register_style( 'mt-snow-status-css', plugins_url('/css/mt-snow-status.css',__FILE__) );

wp_enqueue_style( 'mt-snow-status-css' );
$snowReport = "We have a good base and a foot of new powder";

$DEBUG = false;


function rest_get_info($data)
{
	$options = get_option( 'mt-trail-status-options' );
	
	return new WP_REST_Response( $options, 200 );
}

add_action( 'rest_api_init', function () {
	register_rest_route( 'trail-status-api', '/info', array(
		'methods'         => WP_REST_Server::READABLE,
		'callback'	=> 'rest_get_info'
		) );
});

$default_images = [
		"black"=>$imgdir."trail-black.png",
		"doubleblack"=>$imgdir."trail-doubleblack.png",
		"groomed"=>$imgdir."trail-groomed.png",
		"terrain"=>$imgdir."trail-terrain.png",
		"blue"=>$imgdir."trail-blue.png",
		"event"=>$imgdir."trail-event.png",
		"open"=>$imgdir."trail-open.png",
		"treeski"=>$imgdir."trail-tree.png",
		"closed"=>$imgdir."trail-closed.png",
		"green"=>$imgdir."trail-green.png",
		"snowmaking"=>$imgdir."trail-snowmaking.png",
		"standby"=>$imgdir."trail-standby.png"
	];

function print_status()
{
	$options = get_option( 'mt-trail-status-options' );
	$imagelist = get_option( 'mt-trail-status-icons', $default_images );
	date_default_timezone_set('America/New_York');
	$x = $options['last_update'];
	$last_update = date('D M j, Y g:i a', $x);
	
	$imgdir = plugins_url("/img/",__FILE__);


	$trails = $options["trails"];
	#$imagelist = $options["imagelist"];
	$snowdata = $options["snowdata"];

if($_GET["print"]){
	include( "print.php" );
}
else{

	$output = "<div class=\"mt-snow-status\">"; // shortcode opening div
	$output .= '<span class="last_update">' . $last_update . '</span>';
	$output .= "<div class=\"snow-info\">";
	//Current Snow Conditions:<br/>"; 
	$output .= "<p>" .stripslashes($snowdata["notes"]) . "</p>";
	$output .= "Base: " .$snowdata["base"] . "<br/>"; 
	$output .= "New: ".$snowdata["new"]."<br/>"; 
	$output .= "Open Trails: ".$snowdata["trailsopen"]; 
	$output .= "</div>";

	
	$output .= "<div class=\"trail-info\">";
	foreach ( $trails as $side => $info )
	{
		//$output .= "<h4>" . $side . "</h4>";

		$output .= "<table class=\"trailstatustable\">";
		//$output .= "<tr><th>Trail</th><th>Status</th><th>Groomed</th><th>Snowmaking</th>  </tr>";
		//$output .= "<tr><th colspan=\"5\">" . $side . "</th></tr>";
		$output .= "<caption>" . $side . "</caption>";
		$output .= "<tbody>\n";
			foreach ( $info as $trail => $tdata )
			{	
				$output .= "<tr>";

				//$output .= "<td class=\"skilllevel skill-" .$tdata["skill"] ."\"> </td>";
				$output .= "<td class=\"skilllevel\">";
					$output .= "  <img src=\"" .$imgdir. $imagelist[$tdata["skill"]] . "\"/>";
				$output .= "</td>\n";

				$output .= "<td class=\"trailname\">" . stripslashes($tdata["label"]);
				if( $tdata["groomed"] == 'on' )
				{
					//$output .= "  <img class=\"groomed-img\" src=\"" . $groomed_img . "\" alt=\"groomed\"/>";
					$output .= "  <img class=\"groomed-img\" src=\"" .$imgdir. $imagelist["groomed"] . "\" alt=\"groomed\"/>";
				}
				if( $tdata["snowmaking"] == 'on' )
				{
					$output .= "  <img class=\"snowmaking-img\" src=\"" .$imgdir. $imagelist["snowmaking"] . "\" alt=\"snowmaking\"/>";
				}
				$output .= "</td>\n";

				//$output .= "<td class=\"trailstatus trail-" .  $tdata["status"] . "\"></td>";
				//$output .= "<td class=\"trailstatus\">";
				$output .= "<td>";
					$output .= "  <img src=\"" .$imgdir. $imagelist[$tdata["status"]] . "\" alt=\"".$tdata["status"] ."\" />";
				$output .= "</td>\n";
				
				$output .= "</tr>\n";
			}
		$output .= "</tbody>";
		$output .= "</table>";
		$output .= "</div>\n";

	}
	
	$output .= "<div><ul class=\"trailinfokey\">";
	$output .= "<li><img src=\"" .$imgdir.$imagelist["open"]."\"/> - Trail Open</li>";
	$output .= "<li><img src=\"" .$imgdir.$imagelist["closed"]."\"/> - Trail Closed</li>";
	$output .= "<li><img src=\"" .$imgdir.$imagelist["standby"]."\"/> - Trail on Standby</li>";
	$output .= "<li><img src=\"" .$imgdir.$imagelist["green"]."\"/> - Beginner Trail</li>";
	$output .= "<li><img src=\"" .$imgdir.$imagelist["blue"]."\"/> - Intermediate Trail</li>";
	$output .= "<li><img src=\"" .$imgdir.$imagelist["black"]."\"/> - Advanced Trail</li>";
	$output .= "<li><img src=\"" .$imgdir.$imagelist["doubleblack"]."\"/> - Expert Trail</li>";
	$output .= "<li><img src=\"" .$imgdir.$imagelist["snowmaking"]."\"/> - Snowmaking on Trail</li>";
	$output .= "<li><img src=\"" .$imgdir.$imagelist["groomed"]."\"/> - Trail recently groomed</li>";
	$output .= "</ul></div>";
		

	$output .= "</div>"; // Closing div for shortcode

	$output .= '<a href="?print=1" style="float:right;" target="_blank">print</a>';

	if($DEBUG){	
		$output .= "<pre>";
		$output .= print_r($options, true);
		$output .= "</pre>";
	}
	
	return $output;
}
}

add_action( 'admin_menu', 'add_my_custom_menu' );

function add_my_custom_menu() {
    //add an item to the menu
    add_menu_page (
        'Trail Status',
        'Trail Status',
        'manage_options',
        'mt-trail-status-admin',
        'my_admin_page_function',
        plugin_dir_url( __FILE__ ).'icons/my_icon.png',
        '23.56'
    );
}


function my_admin_page_function() {
/**
 * Created by IntelliJ IDEA.
 * User: jsanford
 * Date: 12/3/15
 * Time: 10:34 PM
 */
$imgdir = plugins_url("/img/",__FILE__);
$options = array();

if ( $_POST['mt-trail-status-action'] == 'update' ){
	$options = $_POST['mt-trail-status-options'];

	$open = 0;
	$trails = $options["trails"];

    	foreach ( $trails as $side => $info ){
        	foreach ( $info as $trail => $tdata ){
			if($tdata['status'] == "open"){
				$open++;
			}			
		}
	}

		
	$options['snowdata']['trailsopen'] = $open;
	
	$options['last_update'] = time();
	update_option( 'mt-trail-status-options', $options );
} 
else{
	$options = get_option( 'mt-trail-status-options' );
}

$imagelist = get_option( 'mt-trail-status-icons', $default_images );

$trails = $options["trails"];
$snowdata = $options["snowdata"];

?>

<div class="wrap">
    <h2>Mt Abram Trail Status</h2>
<form method="post">
	<input type="hidden" name="mt-trail-status-action" value="update"/>
    Base:<input name="mt-trail-status-options[snowdata][base]" type="text" value="<?php echo $snowdata['base'];?>"/><br/>
    New:<input name="mt-trail-status-options[snowdata][new]" type="text" value="<?php echo $snowdata['new'];?>"/><br/>
    Notes:<textarea name="mt-trail-status-options[snowdata][notes]" rows="4" cols="30"><?php echo stripslashes(htmlentities($snowdata['notes'])); ?></textarea><br/>

    <?php

    foreach ( $trails as $side => $info ){
        echo "<h2>" . $side . "</h2>";
        echo "<table>";
        foreach ( $info as $trail => $tdata ){
	    //$namepre = addslashes("mt-trail-status-options[trails][".$side.']['.$trail.']');
	    $namepre = "mt-trail-status-options[trails][".$side.']['.$trail.']';
	    $statusopen = '';
	    $statusclosed = '';
	    $standby = '';
	    $groomed = '';
	    $snowmaking = '';

	    if($tdata['status'] == "open"){ $statusopen = "checked"; }
	    if($tdata['status'] == "closed"){ $statusclosed = "checked"; }
	    if($tdata['status'] == "standby"){ $standby = "checked"; }
            echo "<tr>";
	    echo "<td class=\"skilllevel\">";
            echo "  <img src=\"" .$imgdir. $imagelist[$tdata["skill"]] . "\"/>";
            echo "</td>\n";
	    echo "<td>" .stripslashes($tdata["label"]). "</td>";
	    echo '<td><img src="'.$imgdir.'trail-open.png"/> <input type="radio" name="'.$namepre.'[status]" value="open" '.$statusopen.'/></td>';
            echo "<input type='hidden' name='".$namepre."[skill]' value='".$tdata['skill']. "'/>";
            echo '<input type="hidden" name="'.$namepre.'[label]" value="'.stripslashes($tdata['label']). '"/>';
	    echo "<td><img src=\"".$imgdir."trail-closed.png\"/> <input type='radio' name='".$namepre."[status]' value='closed' ".$statusclosed."/></td>";
	    echo "<td><img src=\"".$imgdir."trail-standby.png\"/> <input type='radio' name='".$namepre."[status]' value='standby' ".$standby."/></td>";
	    if($tdata['groomed'] == 'on' ){$groomed="checked";}
            echo "<td><img src=\"".$imgdir."trail-groomed.png\"/> <input type='checkbox' name='".$namepre."[groomed]' ".$groomed."/></td>";
	    if($tdata['snowmaking'] == 'on'){$snowmaking="checked";}
	    echo "<td><img src=\"".$imgdir."trail-snowmaking.png\"/> <input type='checkbox' name='".$namepre."[snowmaking]' ".$snowmaking."/> </td>";
            echo "</tr>";
        }
        echo "</table>";
    }
    ?>

    <input type="submit"/>
</form>
<hr/>

<h3>Geek Stuff - Please Ignore</h3>
<hr/>
<?php echo "ACTION: " . $_POST['mt-trail-status-action'] . "<br/>"; ?>
<pre>
POST<br/>
<?php print_r($_POST); ?>
</pre>
<hr/>
<pre>
OPTIONS<br/>
<?php print_r($options); ?>
</pre>

</div>
    <?php
}


/*
add_action( 'admin_menu', 'mtstatus_add_options' );
function mtstatus_add_options(){
	add_option('mt-trail-status-options', 'Mt-Trail-Status-Options');
}
*/

if($_GET["reset_trails"]){
	add_action( 'admin_menu', 'mtstatus_update_options' );
}

function mtstatus_update_options(){


        $options = [
		"trails" => [
			"Main Side" => [
				"LowerRound-a-bout" => ["status"=>"closed","groomed"=>"0","snowmaking"=>"0","skill"=>"green","label"=>"Lower Round-a-bout"],
				"WilksWay" => ["status"=>"closed","groomed"=>"0","snowmaking"=>"0","skill"=>"green","label"=>"Wilk's Way"],
				"Upsidasium" => ["status"=>"closed","groomed"=>"0","snowmaking"=>"0","skill"=>"green","label"=>"Upsidasium"],
				"OneToday" => ["status"=>"closed","groomed"=>"0","snowmaking"=>"0","skill"=>"green","label"=>"One Today"],
				"LowerEasyRider" => ["status"=>"closed","groomed"=>"0","snowmaking"=>"0","skill"=>"green","label"=>"Lower Easy Rider"],
				"TaylorsTreat" => ["status"=>"closed","groomed"=>"0","snowmaking"=>"0","skill"=>"green","label"=>"Taylor's Treat"],

				"UpperSweeper" => ["status"=>"closed","groomed"=>"0","snowmaking"=>"0","skill"=>"blue","label"=>"Upper Sweeper"],
				"Sweeper" => ["status"=>"closed","groomed"=>"0","snowmaking"=>"0","skill"=>"blue","label"=>"Sweeper"],
				"FrostbiteFalls" => ["status"=>"closed","groomed"=>"0","snowmaking"=>"0","skill"=>"blue","label"=>"Frostbite Falls"],
				"R2D2" => ["status"=>"closed","groomed"=>"0","snowmaking"=>"0","skill"=>"blue","label"=>"R2D2"],
				"BullwinklesHorn" => ["status"=>"closed","groomed"=>"0","snowmaking"=>"0","skill"=>"blue","label"=>"Bullwinkle's Horn"],
				"BullwinklesOtherHorn" => ["status"=>"closed","groomed"=>"0","snowmaking"=>"0","skill"=>"blue","label"=>"Bullwinkle's Other Horn"],
				"UpperEasyRider" => ["status"=>"closed","groomed"=>"0","snowmaking"=>"0","skill"=>"blue","label"=>"Upper Easy Rider"],
				"Snowfields" => ["status"=>"closed","groomed"=>"0","snowmaking"=>"0","skill"=>"blue","label"=>"Snowfields"],
				"Snake" => ["status"=>"closed","groomed"=>"0","snowmaking"=>"0","skill"=>"blue","label"=>"Snake"],
				"UpperBullwinkle" => ["status"=>"closed","groomed"=>"0","snowmaking"=>"0","skill"=>"blue","label"=>"Upper Bullwinkle"],
				"Bullwinkle" => ["status"=>"closed","groomed"=>"0","snowmaking"=>"0","skill"=>"blue","label"=>"Bullwinkle"],
				"Dudley–Do-Right" => ["status"=>"closed","groomed"=>"0","snowmaking"=>"0","skill"=>"blue","label"=>"Dudley-Do-Right"],
				"DudleyPark" => ["status"=>"closed","groomed"=>"0","snowmaking"=>"0","skill"=>"blue","label"=>"Dudley Park"],
				"MaybellesTail" => ["status"=>"closed","groomed"=>"0","snowmaking"=>"0","skill"=>"blue","label"=>"Maybelle's Tail"],
				"CrossStreet" => ["status"=>"closed","groomed"=>"0","snowmaking"=>"0","skill"=>"blue","label"=>"Cross Street"],
				"NellsWay" => ["status"=>"closed","groomed"=>"0","snowmaking"=>"0","skill"=>"blue","label"=>"Nell's Way"],
				"PatsPass" => ["status"=>"closed","groomed"=>"0","snowmaking"=>"0","skill"=>"blue","label"=>"Pat's Pass"],
				"WassomattaU" => ["status"=>"closed","groomed"=>"0","snowmaking"=>"0","skill"=>"blue","label"=>"WassomattaU"],
				"HoppityHooper" => ["status"=>"closed","groomed"=>"0","snowmaking"=>"0","skill"=>"blue","label"=>"Hoppity Hooper"],
				"Round-a-Bout" => ["status"=>"closed","groomed"=>"0","snowmaking"=>"0","skill"=>"blue","label"=>"Round-a-Bout"],
				"BigT-Line" => ["status"=>"closed","groomed"=>"0","snowmaking"=>"0","skill"=>"blue","label"=>"BigT-Line"],
				"PeabodysPass" => ["status"=>"closed","groomed"=>"0","snowmaking"=>"0","skill"=>"blue","label"=>"Peabody's Pass"],
				"ChickenChute" => ["status"=>"closed","groomed"=>"0","snowmaking"=>"0","skill"=>"blue","label"=>"Chicken Chute"],
				"ShermansPass" => ["status"=>"closed","groomed"=>"0","snowmaking"=>"0","skill"=>"blue","label"=>"Sherman's Pass"],
				"NatashasNiche" => ["status"=>"closed","groomed"=>"0","snowmaking"=>"0","skill"=>"blue","label"=>"Natasha's Niche"],
				"Presto" => ["status"=>"closed","groomed"=>"0","snowmaking"=>"0","skill"=>"blue","label"=>"Presto"],
				"McClainsLane" => ["status"=>"closed","groomed"=>"0","snowmaking"=>"0","skill"=>"blue","label"=>"McClain's Lane"],

				"UpperFracturedFairytales" => ["status"=>"closed","groomed"=>"1","snowmaking"=>"1","skill"=>"black","label"=>"Upper Fractured Fairytales"],
				"LowerFracturedFairytales" => ["status"=>"closed","groomed"=>"1","snowmaking"=>"1","skill"=>"black","label"=>"Lower Fractured Fairytales"],
				"Lallypalooza" => ["status"=>"closed","groomed"=>"1","snowmaking"=>"1","skill"=>"black","label"=>"Lallypalooza"],
				"TuckersTumble" => ["status"=>"closed","groomed"=>"1","snowmaking"=>"1","skill"=>"black","label"=>"Tucker's Tumble"],
				"UpperRockysRun" => ["status"=>"closed","groomed"=>"1","snowmaking"=>"1","skill"=>"black","label"=>"Upper Rocky's Run"],
				"LowerRockysRun" => ["status"=>"closed","groomed"=>"1","snowmaking"=>"1","skill"=>"black","label"=>"Lower Rocky's Run"],
				"SnidleyWhiplash" => ["status"=>"closed","groomed"=>"1","snowmaking"=>"1","skill"=>"black","label"=>"Snidley Whiplash"],
				"FearlessLeader" => ["status"=>"closed","groomed"=>"1","snowmaking"=>"1","skill"=>"black","label"=>"Fearless Leader"],
				"BorisBadenov" => ["status"=>"closed","groomed"=>"1","snowmaking"=>"1","skill"=>"black","label"=>"Boris Badenov"],
				"ZephyrGlades" => ["status"=>"closed","groomed"=>"1","snowmaking"=>"1","skill"=>"black","label"=>"Zephyr Glades"],
				"LowerZephyrGlades" => ["status"=>"closed","groomed"=>"1","snowmaking"=>"1","skill"=>"black","label"=>"Lower Zephyr Glades"],
				"TheRAT" => ["status"=>"closed","groomed"=>"1","snowmaking"=>"1","skill"=>"black","label"=>"The RAT"],

				"TheCliff" => ["status"=>"closed","groomed"=>"0","snowmaking"=>"0","skill"=>"doubleblack","label"=>"The Cliff"],
				"TheZone" => ["status"=>"closed","groomed"=>"0","snowmaking"=>"0","skill"=>"doubleblack","label"=>"The Zone"]

			],
			"West Side" => [
				"Hayroad" => ["status"=>"closed","groomed"=>"1","snowmaking"=>"0","skill"=>"green","label"=>"Hayroad"],
				"Skyline" => ["status"=>"closed","groomed"=>"1","snowmaking"=>"0","skill"=>"green","label"=>"Skyline"],
				"MahoosucMeadow" => ["status"=>"closed","groomed"=>"1","snowmaking"=>"0","skill"=>"green","label"=>"Mahoosuc Meadow"],
				"Polkadot" => ["status"=>"closed","groomed"=>"1","snowmaking"=>"0","skill"=>"green","label"=>"Polkadot"],
				"GidneysGulch" => ["status"=>"closed","groomed"=>"1","snowmaking"=>"0","skill"=>"green","label"=>"Gidney's Gulch"],

				"Egomaineah" => ["status"=>"closed","groomed"=>"1","snowmaking"=>"0","skill"=>"blue","label"=>"Egomaineah"],
				"FlyingSquirrelTerrainPark" => ["status"=>"closed","groomed"=>"1","snowmaking"=>"0","skill"=>"blue","label"=>"Flying Squirrel Terrain Park"]
			]
		],
		"snowdata" => [
			"base" => "0",
			"new" => 0,
			"notes" => ""
		]
	];

	update_option( 'mt-trail-status-options', $options );
}

if($_GET["update_icons"]){
	//add_action( 'admin_menu', 'mtstatus_update_options' );

		$imagelist = [
			"black"=>"trail-black.png",
			"doubleblack"=>"trail-doubleblack.png",
			"groomed"=>"trail-groomed.png",
			"terrain"=>"trail-terrain.png",
			"blue"=>"trail-blue.png",
			"event"=>"trail-event.png",
			"open"=>"trail-open.png",
			"treeski"=>"trail-tree.png",
			"closed"=>"trail-closed.png",
			"standby"=>"trail-standby.png",
			"green"=>"trail-green.png",
			"snowmaking"=>"trail-snowmaking.png"
		];
	update_option( 'mt-trail-status-icons', $imagelist);
}
/*
add_action( 'admin_menu', 'wporg_custom_admin_menu' );
 
function wporg_custom_admin_menu() {
    add_options_page(
        'My Plugin Title',
        'My Plugin Menu Item',
        'manage_options',
        'wporg-plugin',
        'wporg_options_page'
    );
}
*/

function mtstatus_check_for_page_tree() {
 
    //start by checking if we're on a page
    if( is_page() ) {
     
        global $post;
     
        // next check if the page has parents
        if ( $post->post_parent ){
         
            // fetch the list of ancestors
            $parents = array_reverse( get_post_ancestors( $post->ID ) );
             
            // get the top level ancestor
            return $parents[0];
             
        }
         
        // return the id  - this will be the topmost ancestor if there is one, or the current page if not
        return $post->ID;
    }
}

/*
"Hayroad" => ["status"=>"closed","groomed"=>"1","snowmaking"=>"0","skill"=>"green"],

if( $tdata["groomed"] == 'on' )
{
	$output .= "  <img class=\"groomed-img\" src=\"" .$imgdir. $imagelist["groomed"] . "\" alt=\"groomed\"/>";
}
if( $tdata["snowmaking"] == 'on' )
{
	$output .= "  <img class=\"snowmaking-img\" src=\"" .$imgdir. $imagelist["snowmaking"] . "\" alt=\"snowmaking\"/>";
}

*/

function trail_print_row( $side, $tname ){

	$options = get_option( 'mt-trail-status-options' );
	$imagelist = get_option( 'mt-trail-status-icons', $default_images );
	
	$imgdir = plugins_url("/img/",__FILE__);


	$trails = $options["trails"];
	#$imagelist = $options["imagelist"];
	$snowdata = $options["snowdata"];


/*	$return = '<td class="skilllevelprint"><img src="'.$imgdir. $imagelist[ $trails[$side][$tname]["skill"] ] .'"/></td>
		   <td class="trailnameprint">'.$tname;*/

	$return = '<td class="trailnameprint"><img src="'.$imgdir. $imagelist[ $trails[$side][$tname]["skill"] ] .'"/> '.stripslashes($trails[$side][$tname]["label"]);

	if( $trails[$side][$tname]["groomed"] == 'on' )
	{
		$return .= '<img class="groomed-img" src="' .$imgdir. $imagelist["groomed"] . '" alt="groomed"/>';
	}
	if( $trails[$side][$tname]["snowmaking"] == 'on' )
	{
		$return .= '<img class="snowmaking-img" src="' .$imgdir. $imagelist["snowmaking"] . '" alt="snowmaking"/>';
		//$return .= "  <img src=\"" .$imgdir. $imagelist["snowmaking"] . "\" alt=\"snowmaking\"/>";
	}

	$return .= '<img src="'. $imgdir. $imagelist[ $trails[$side][$tname]["status"] ].'" style="float:right; margin-right:2px"/>';
	$return .= '</td>';
	//$return .= '<td class="trailstatusprint"><img src="'. $imgdir. $imagelist[ $trails[$side][$tname]["status"] ].'"/></td>';
	return $return;
}


class Mt_Snow_Status_Widget extends WP_Widget {
     

    function __construct() {
	parent::__construct(
         
        // base ID of the widget
        'mt_snow_status_widget',
         
        // name of the widget
        __('Trail Status', 'mt_snow_status' ),
         
        // widget options
        array (
            'description' => __( 'Displays short summary of mountain snow status.', 'mt_snow_status' )
        )
         
    );
    }
     
    function form( $instance ) {
    }
     
    function update( $new_instance, $old_instance ) {       
    }
     
    function widget( $args, $instance ) {
    	// kick things off
    $options = get_option( 'mt-trail-status-options' );
    $snowdata = $options['snowdata'];
    extract( $args );
    echo $before_widget;        
    echo $before_title . 'Trail Status:' . $after_title;     
	
	date_default_timezone_set('America/New_York');
	
	$t = date('D M j, Y g:i a', $options['last_update']);
	echo $t;
    //echo "Here is my snow information";      
	echo "<p>" .stripslashes($snowdata["notes"]) . "</p>";
	echo "Base: " .$snowdata["base"] . "<br/>"; 
	echo "New: ".$snowdata["new"] . "<br/>"; 
	echo "Open Trails: ".$snowdata["trailsopen"] . "<br/>"; 
	echo "<a href='/index.php/about/trail-report/'>Full Trail Report<a>";
    }
     
}

function mtstatus_register_status_widget() {
 
    register_widget( 'Mt_Snow_Status_Widget' );
 
}
add_action( 'widgets_init', 'mtstatus_register_status_widget' );
?>
